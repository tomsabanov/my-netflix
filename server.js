const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors')

const app = express();
const port = process.env.PORT || 5000;
const hostname = "127.0.0.1";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());


const movieFolder = "/home/tom/movies/";
const fs = require('fs');

// API calls
app.get('/api/movies', (req, res) => {
  //search the directory for all movies
  var movies = [];
  fs.readdir(movieFolder, (err, files) => {
    files.forEach(file => {
      if(file != "default.png"){
        movies.push(file);      
      }
    });
    res.send(movies);
  });
});

app.get('/api/movie/:movie/image', (req,res) => {
  var movie = req.params.movie;
  var img_jpg = path.join(movieFolder, movie, 'img.jpg');
  var img_png = path.join(movieFolder, movie, 'img.png');
  var img_default = path.join(movieFolder, 'default.png');

  if (fs.existsSync(img_jpg)) {
    res.sendFile(img_jpg);
  }
  else if(fs.existsSync(img_png)){
    res.sendFile(img_png);
  }
  else{
    res.sendFile(img_default);
  }
})

app.get('/api/movie/:movie', (req, res) => {

    const movie_path = path.join(movieFolder, req.params.movie,req.params.movie + ".mp4");
    const stat = fs.statSync(movie_path)
    const fileSize = stat.size
    const range = req.headers.range
    if (range) {
      const parts = range.replace(/bytes=/, "").split("-")
      const start = parseInt(parts[0], 10)
      const end = parts[1] 
        ? parseInt(parts[1], 10)
        : fileSize-1
      const chunksize = (end-start)+1
      const file = fs.createReadStream(movie_path, {start, end})
      const head = {
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(206, head);
      file.pipe(res);
    } else {
      const head = {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
      }
      res.writeHead(200, head)
      fs.createReadStream(movie_path).pipe(res)
    }
})


app.listen(port, hostname, () => console.log(`Listening on port ${port}`));