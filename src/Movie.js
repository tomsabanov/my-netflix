import React, { Component } from 'react';
import './App.css';
import {Link} from 'react-router-dom';
import {Button} from 'react-bootstrap';

export class Movie extends Component {

    constructor(props){
        super(props);
        this.state = {
            url: 'http://127.0.0.1:5000/api/movie/'+props.match.params.movie,
            movie: props.match.params.movie
        }
    }
      
    render() {
        return (
        <div className="App">
            <header className="App-header">
                 <Link to="/">
                     <Button variant="primary">Back</Button>
                 </Link>
                <video id="videoPlayer" controls> 
                    <source src={this.state.url} type="video/mp4"/>
                </video>
            </header>
        </div>
        );
    }
}
