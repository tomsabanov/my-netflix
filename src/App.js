import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import {Movies} from './Movies.js'
import {Movie} from './Movie.js'

class App extends Component {
  
render() {
    return (
      <Router>
        <Route path="/" exact component={Movies} />
        <Route path="/movie/:movie" component={Movie} />
      </Router>
    );
  }
}

export default App;