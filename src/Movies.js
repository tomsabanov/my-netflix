import React, { Component } from 'react';
import './App.css';
import { Card, CardDeck } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export class Movies extends Component {
  state = {
    url: 'http://127.0.0.1:5000/api',
    movies: [],
  };
  
  componentDidMount() {
    this.getMovies()
      .then(movies => this.setState({ movies: movies }))
      .catch(err => console.log(err));
  }
  
  getMovies = async () => {
    var api_url = this.state.url + "/movies";
    const response = await fetch(api_url);
    const movies = await response.json();
    if (response.status !== 200) return [];
    return movies;
  };
  
  displayMovies(){
    const {url, movies} = this.state;

    var list = movies.map((movie) => 
        <Link to={"/movie/" + movie}>
            <Card style={{ width: '20rem' }} text="dark">
                <Card.Img  src={url + "/movie/"+movie+"/image"} />
                <Card.Body>
                    <Card.Title>{movie}</Card.Title>
                    <Card.Text style={{text:"black"}}>
                    Opis filma
                    </Card.Text>
                </Card.Body>
            </Card>
        </Link>
    )

    return list;
  }
  
render() {
    return (
      <div className="App">
        <header className="App-header">
            <h1>My Shitty Netflix</h1>
          <div>
            <CardDeck>
            {this.displayMovies.bind(this)()}
            </CardDeck>
          </div>         
        </header>
      </div>
    );
  }
}
